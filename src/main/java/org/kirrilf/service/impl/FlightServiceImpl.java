package org.kirrilf.service.impl;


import lombok.RequiredArgsConstructor;
import org.kirrilf.dto.request.FindFlightRequest;
import org.kirrilf.dto.request.FindQueryType;
import org.kirrilf.dto.request.FlightRequest;
import org.kirrilf.dto.response.FindFlightResponse;
import org.kirrilf.dto.response.FlightResponse;
import org.kirrilf.exception.ApiRequestException;
import org.kirrilf.exception.ErrorCode;
import org.kirrilf.model.Flight;
import org.kirrilf.model.FlightStatus;
import org.kirrilf.model.SpacePort;
import org.kirrilf.model.Status;
import org.kirrilf.repository.FlightRepository;
import org.kirrilf.repository.ShuttleRepository;
import org.kirrilf.repository.SpacePortRepository;
import org.kirrilf.service.FlightService;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;


@Service
@RequiredArgsConstructor
public class FlightServiceImpl implements FlightService {

    private final FlightRepository flightRepository;

    private final SpacePortRepository spacePortRepository;

    private final ShuttleRepository shuttleRepository;


    @Override
    public FlightResponse bookSeats(String flightCode, Integer count) {
        Flight flight = flightRepository.findFlightByFlightCode(flightCode).orElseThrow(() -> new ApiRequestException(ErrorCode.WRONG_SEARCH_PARAM.toString()));
        if (flight.getRemainSeats() < count) {
            throw new ApiRequestException(ErrorCode.NOT_ENOUGH_SEATS.toString());
        }
        flight.setRemainSeats(flight.getRemainSeats() - count);
        return mapToResponse(flightRepository.save(flight));
    }

    @Override
    public FlightResponse removeBookSeats(String flightCode, Integer count) {
        Flight flight = flightRepository.findFlightByFlightCode(flightCode).orElseThrow(() -> new ApiRequestException(ErrorCode.WRONG_SEARCH_PARAM.toString()));
        if (shuttleRepository.findShuttleByShuttleCode(flight.getShuttle().getShuttleCode())
                .orElseThrow(() -> new ApiRequestException(ErrorCode.SHUTTLE_WITH_THIS_NAME_MISSING.toString()))
                .getRange() < flight.getRemainSeats() + count) {
            throw new ApiRequestException(ErrorCode.TOO_MANY_REMAIN_SEATS.toString());
        }
        flight.setRemainSeats(flight.getRemainSeats() + count);
        return mapToResponse(flightRepository.save(flight));
    }

    @Override
    public FlightResponse getFlightByFlightNumber(String flightNumber) {
        return mapToResponse(flightRepository.findFlightByFlightCode(flightNumber).orElseThrow(() -> new ApiRequestException(ErrorCode.WRONG_SEARCH_PARAM.toString())));
    }

    @Override
    public FlightResponse createFlight(FlightRequest flightRequest) {
        return mapToResponse(flightRepository.save(buildFlight(flightRequest)));
    }

    @Override
    public Boolean flightValidation(List<String> flightCodes) {
        List<Flight> flights = flightRepository.findAll();
        List<String> invalidFlightCodes = flightCodes.stream()
                .filter(it -> checkFlightCode(flights, it))
                .collect(Collectors.toList());
        if (invalidFlightCodes.isEmpty()) {
            return true;
        }
        throw new ApiRequestException(invalidFlightCodes.toString());

    }

    @Override
    public FindFlightResponse findFlightsByParams(FindFlightRequest request) {
        List<SpacePort> from = findSpacePortByParam(request.getFrom(), request.getType());
        List<SpacePort> to = findSpacePortByParam(request.getTo(), request.getType());
        return FindFlightResponse.builder()
                .flightsTo(
                        getFlightsBySpacePorts(from, to,
                                request.getWhen(), request.getDateRangeTo() == null ? request.getWhen() : request.getDateRangeTo()).stream().map(this::mapToResponse).collect(Collectors.toList()))
                .flightsBack(request.isNeedTicketBack() ?
                        getFlightsBySpacePorts(to, from,
                                request.getWhenBack(), request.getDateRangeBack() == null ? request.getWhenBack() : request.getDateRangeBack()).stream().map(this::mapToResponse).collect(Collectors.toList())
                        : null)
                .build();

    }

    private List<SpacePort> findSpacePortByParam(String param, FindQueryType type) {
        switch (type) {
            case Planet:
                return spacePortRepository.findSpacePortByPlanetName(param).orElseThrow(() -> new ApiRequestException(ErrorCode.WRONG_SEARCH_PARAM.toString()));
            case SpacePortCode:
                return List.of(spacePortRepository.findSpacePortBySpacePortCode(param).orElseThrow(() -> new ApiRequestException(ErrorCode.WRONG_SEARCH_PARAM.toString())));
            case SpacePortName:
                return List.of(spacePortRepository.findSpacePortBySpacePortName(param).orElseThrow(() -> new ApiRequestException(ErrorCode.WRONG_SEARCH_PARAM.toString())));
            default:
                throw new ApiRequestException(ErrorCode.WRONG_FIND_PARAMETERS.toString());
        }
    }

    private List<Flight> getFlightsBySpacePorts(List<SpacePort> from, List<SpacePort> to, LocalDate when, LocalDate rangeEnd) {
        return flightRepository.findFlightsBySpacePort(
                from,
                to,
                localDateTimeToDate(when.atTime(LocalTime.MIN)),
                localDateTimeToDate(rangeEnd.atTime(LocalTime.MAX))
        ).stream().filter(it -> it.getRemainSeats() > 0).collect(Collectors.toList());
    }


    private static Date localDateTimeToDate(LocalDateTime localDateTime) {
        return Date.from(localDateTime.atZone(ZoneId.systemDefault()).toInstant());
    }

    private Boolean checkFlightCode(List<Flight> flights, String flightCode) {
        return flights.stream().noneMatch(it -> it.getFlightCode().equals(flightCode));
    }

    private FlightResponse mapToResponse(Flight flight) {
        return FlightResponse.builder()
                .actualArrivalTime(flight.getActualArrivalTime())
                .actualDepartureTime(flight.getActualDepartureTime())
                .scheduleDepartureTime(flight.getScheduleDepartureTime())
                .scheduleArrivalTime(flight.getScheduleArrivalTime())
                .shuttleCode(flight.getShuttle().getShuttleCode())
                .flightStatus(flight.getFlightStatus().name())
                .spacePortCodeFrom(flight.getSpacePortFrom().getSpacePortCode())
                .spacePortCodeTo(flight.getSpacePortTo().getSpacePortCode())
                .flightCode(flight.getFlightCode())
                .price(flight.getPrice())
                .build();
    }

    private Flight buildFlight(FlightRequest flightRequest) {
        Flight flight = new Flight();
        flight.setSpacePortFrom(spacePortRepository.findSpacePortBySpacePortCode(flightRequest.getSpacePortCodeFrom()).orElseThrow(() -> new ApiRequestException(ErrorCode.WRONG_SEARCH_PARAM.toString())));
        flight.setSpacePortTo(spacePortRepository.findSpacePortBySpacePortCode(flightRequest.getSpacePortCodeTo()).orElseThrow(() -> new ApiRequestException(ErrorCode.WRONG_SEARCH_PARAM.toString())));
        flight.setFlightCode(flightRequest.getFlightCode());
        flight.setShuttle(shuttleRepository.findShuttleByShuttleCode(flightRequest.getShuttleCode())
                .orElseThrow(() -> new ApiRequestException(ErrorCode.SHUTTLE_WITH_THIS_NAME_MISSING.toString())));
        flight.setActualArrivalTime(new Date(flightRequest.getArrivalTime() * 1000));
        flight.setActualDepartureTime(new Date(flightRequest.getDepartureTime() * 1000));
        flight.setScheduleArrivalTime(new Date(flightRequest.getArrivalTime() * 1000));
        flight.setScheduleDepartureTime(new Date(flightRequest.getDepartureTime() * 1000));
        flight.setPrice(flightRequest.getPrice());
        flight.setFlightStatus(FlightStatus.SCHEDULED);
        flight.setCreated(new Date());
        flight.setUpdated(new Date());
        flight.setStatus(Status.ACTIVE);
        flight.setRemainSeats(shuttleRepository.findShuttleByShuttleCode(flightRequest.getShuttleCode())
                .orElseThrow(() -> new ApiRequestException(ErrorCode.SHUTTLE_WITH_THIS_NAME_MISSING.toString()))
                .getRange());
        return flight;
    }


}
