package org.kirrilf.service.impl;


import lombok.RequiredArgsConstructor;
import org.kirrilf.dto.request.ShuttleRequest;
import org.kirrilf.dto.response.ShuttleResponse;
import org.kirrilf.exception.ApiRequestException;
import org.kirrilf.exception.ErrorCode;
import org.kirrilf.model.*;
import org.kirrilf.repository.SeatRepository;
import org.kirrilf.repository.ShuttleRepository;
import org.kirrilf.service.ShuttleService;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class ShuttleServiceImpl implements ShuttleService {

    private final ShuttleRepository shuttleRepository;

    private final SeatRepository seatRepository;

    @Override
    public ShuttleResponse getShuttleByName(String shuttleName, Boolean withSeats) {
        Shuttle shuttle = shuttleRepository.findShuttleByShuttleName(shuttleName)
                .orElseThrow(() -> new ApiRequestException(ErrorCode.SHUTTLE_WITH_THIS_NAME_MISSING.toString()));
        ShuttleResponse response = mapToResponse(shuttle);
        if(withSeats)
            response.setSeats(seatRepository.findAllByShuttle(shuttle)
                    .stream().map(String::valueOf).collect(Collectors.toList()));
        return response;
    }

    @Override
    public ShuttleResponse getShuttleByCode(String shuttleCode) {
        return mapToResponse(shuttleRepository.findShuttleByShuttleCode(shuttleCode)
                .orElseThrow(() -> new ApiRequestException(ErrorCode.SHUTTLE_WITH_THIS_NAME_MISSING.toString())));
    }

    @Override
    @Transactional
    public ShuttleResponse createShuttle(ShuttleRequest shuttleRequest) {
        Shuttle shuttle = shuttleRepository.save(buildShuttle(shuttleRequest));
        seatRepository.saveAll(getSeatsFromRequest(shuttleRequest, shuttle));
        return mapToResponse(shuttle);
    }

    private List<Seat> getSeatsFromRequest(ShuttleRequest shuttleRequest, Shuttle shuttle){
        return shuttleRequest.getSeats()
                .stream()
                .map(this::buildSeat)
                .peek(x -> x.setShuttle(shuttle))
                .collect(Collectors.toList());
    }

    private Seat buildSeat(String seatNumber){
        Seat seat = new Seat();
        seat.setSeat(seatNumber);
        seat.setSeatStatus(SeatStatus.FREE);
        seat.setCreated(new Date());
        seat.setUpdated(new Date());
        seat.setStatus(Status.ACTIVE);
        return seat;
    }

    private ShuttleResponse mapToResponse(Shuttle shuttle){
        return ShuttleResponse.builder()
                .shuttleModel(shuttle.getModel().name())
                .shuttleRange(shuttle.getRange())
                .shuttleCode(shuttle.getShuttleCode())
                .shuttleName(shuttle.getShuttleName())
                .build();
    }

    private Shuttle buildShuttle(ShuttleRequest shuttleRequest){
        Shuttle shuttle = new Shuttle();
        shuttle.setShuttleCode(shuttleRequest.getShuttleCode());
        shuttle.setShuttleName(shuttleRequest.getShuttleName());
        shuttle.setRange(shuttleRequest.getShuttleRange());
        shuttle.setModel(ShuttleModel.valueOf(shuttleRequest.getShuttleModel()));
        shuttle.setCreated(new Date());
        shuttle.setUpdated(new Date());
        shuttle.setStatus(Status.ACTIVE);
        return shuttle;
    }

}
