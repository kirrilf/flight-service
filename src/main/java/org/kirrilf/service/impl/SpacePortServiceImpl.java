package org.kirrilf.service.impl;


import lombok.RequiredArgsConstructor;
import org.kirrilf.dto.request.SpacePortRequest;
import org.kirrilf.dto.response.SpacePortResponse;
import org.kirrilf.exception.ApiRequestException;
import org.kirrilf.exception.ErrorCode;
import org.kirrilf.model.SpacePort;
import org.kirrilf.model.Status;
import org.kirrilf.repository.SpacePortRepository;
import org.kirrilf.service.SpacePortService;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class SpacePortServiceImpl implements SpacePortService {

    private final SpacePortRepository spacePortRepository;

    @Override
    public SpacePortResponse createSpacePort(SpacePortRequest spacePort) {
        return mapToResponse(spacePortRepository.save(buildSpacePort(spacePort)));
    }

    @Override
    public SpacePortResponse getSpacePortBySpacePortCode(String spacePortCode) {
        return mapToResponse(spacePortRepository.findSpacePortBySpacePortCode(spacePortCode).orElseThrow(() -> new ApiRequestException(ErrorCode.WRONG_SEARCH_PARAM.toString())));
    }

    @Override
    public SpacePortResponse getSpacePortBySpacePortName(String spacePortName) {
        return mapToResponse(spacePortRepository.findSpacePortBySpacePortName(spacePortName).orElseThrow(() -> new ApiRequestException(ErrorCode.WRONG_SEARCH_PARAM.toString())));
    }

    @Override
    public List<SpacePortResponse> getSpacePortsByPlanetName(String planetName) {
        return spacePortRepository.findSpacePortByPlanetName(planetName)
                .orElseThrow(() -> new ApiRequestException(ErrorCode.WRONG_SEARCH_PARAM.toString()))
                .stream().map(this::mapToResponse
                ).collect(Collectors.toList());
    }

    private SpacePortResponse mapToResponse(SpacePort spacePort) {
        return SpacePortResponse.builder()
                .spacePortName(spacePort.getSpacePortName())
                .spacePortCode(spacePort.getSpacePortCode())
                .planetName(spacePort.getPlanetName())
                .build();
    }

    private SpacePort buildSpacePort(SpacePortRequest spacePortRequest) {
        SpacePort spacePort = new SpacePort();
        spacePort.setSpacePortCode(spacePortRequest.getSpacePortCode());
        spacePort.setSpacePortName(spacePortRequest.getSpacePortName());
        spacePort.setPlanetName(spacePortRequest.getPlanetName());
        spacePort.setCreated(new Date());
        spacePort.setUpdated(new Date());
        spacePort.setStatus(Status.ACTIVE);
        return spacePort;
    }


}
