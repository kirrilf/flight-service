package org.kirrilf.service;


import org.kirrilf.dto.request.ShuttleRequest;
import org.kirrilf.dto.response.ShuttleResponse;

public interface ShuttleService {

    ShuttleResponse getShuttleByName(String shuttleName, Boolean withSeats);

    ShuttleResponse getShuttleByCode(String shuttleCode);

    ShuttleResponse createShuttle(ShuttleRequest shuttleRequest);

}
