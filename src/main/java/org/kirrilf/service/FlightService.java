package org.kirrilf.service;


import org.kirrilf.dto.request.FindFlightRequest;
import org.kirrilf.dto.request.FlightRequest;
import org.kirrilf.dto.response.FindFlightResponse;
import org.kirrilf.dto.response.FlightResponse;

import java.util.List;

public interface FlightService {

    FlightResponse bookSeats(String flightCode, Integer count);

    FlightResponse removeBookSeats(String flightCode, Integer count);

    FlightResponse getFlightByFlightNumber(String flightNumber);

    FlightResponse createFlight(FlightRequest flightRequest);

    Boolean flightValidation(List<String> flightCodes);

    FindFlightResponse findFlightsByParams(FindFlightRequest request);


}
