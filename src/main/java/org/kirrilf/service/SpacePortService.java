package org.kirrilf.service;


import org.kirrilf.dto.request.SpacePortRequest;
import org.kirrilf.dto.response.SpacePortResponse;

import java.util.List;

public interface SpacePortService {

    SpacePortResponse createSpacePort(SpacePortRequest spacePort);

    SpacePortResponse getSpacePortBySpacePortCode(String spacePortCode);

    SpacePortResponse getSpacePortBySpacePortName(String spacePortName);

    List<SpacePortResponse> getSpacePortsByPlanetName(String planetName);


}
