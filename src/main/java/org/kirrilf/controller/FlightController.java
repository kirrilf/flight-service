package org.kirrilf.controller;


import lombok.RequiredArgsConstructor;
import org.kirrilf.dto.request.FindFlightRequest;
import org.kirrilf.dto.request.FindQueryType;
import org.kirrilf.dto.request.FlightRequest;
import org.kirrilf.dto.response.FindFlightResponse;
import org.kirrilf.dto.response.FlightResponse;
import org.kirrilf.service.FlightService;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;

@RestController
@RequestMapping("/flight")
@RequiredArgsConstructor
public class FlightController {


    private final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");

    private final FlightService flightService;

    @GetMapping("/{flightCode}")
    public FlightResponse getFlight(@PathVariable String flightCode) {
        return flightService.getFlightByFlightNumber(flightCode);
    }


    @GetMapping()
    public FindFlightResponse getFlightsByFromTo(@RequestParam String from,
                                                 @RequestParam String to,
                                                 @RequestParam String when,
                                                 @RequestParam(required = false, name = "date-range-to") String dateRangeTo,
                                                 @RequestParam(required = false, name = "date-range-back") String dateRangeBack,
                                                 @RequestParam FindQueryType type,
                                                 @RequestParam(required = false) String whenBack) {

        FindFlightRequest request = FindFlightRequest.builder()
                .from(from)
                .to(to)
                .type(type)
                .when(LocalDate.parse(when, formatter))
                .build();
        if (whenBack != null) {
            request.setNeedTicketBack(true);
            request.setWhenBack(LocalDate.parse(whenBack, formatter));
        }
        if(dateRangeTo != null){
            request.setDateRangeTo(LocalDate.parse(dateRangeTo, formatter));
        }
        if(dateRangeBack != null){
            request.setDateRangeBack(LocalDate.parse(dateRangeBack, formatter));
        }

        return flightService.findFlightsByParams(request);

    }




    @GetMapping("/book-seats/{flightCode}/{count}")
    public FlightResponse bookSeats(@PathVariable("flightCode") String flightCode,
                                    @PathVariable("count") Integer count) {
        return flightService.bookSeats(flightCode, count);
    }

    @GetMapping("/remove-book-seats/{flightCode}/{count}")
    public FlightResponse removeBookSeats(@PathVariable("flightCode") String flightCode,
                                          @PathVariable("count") Integer count) {
        return flightService.removeBookSeats(flightCode, count);
    }

    @PostMapping
    public FlightResponse createFlight(@RequestBody FlightRequest flightRequest) {
        return flightService.createFlight(flightRequest);
    }

    @PostMapping("/validation")
    public Boolean flightsValidation(@RequestBody List<String> flightCodes) {
        return flightService.flightValidation(flightCodes);
    }

}
