package org.kirrilf.controller;


import lombok.RequiredArgsConstructor;
import org.kirrilf.dto.request.SpacePortRequest;
import org.kirrilf.dto.response.SpacePortResponse;
import org.kirrilf.service.SpacePortService;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/space-port")
@RequiredArgsConstructor
public class SpacePortController {

    private final SpacePortService spacePortService;

    @GetMapping("/{spacePortName}")
    public SpacePortResponse getSpacePort(@PathVariable String spacePortName){
        return spacePortService.getSpacePortBySpacePortName(spacePortName);
    }

    @GetMapping()
    public List<SpacePortResponse> getSpacePortsByPlanetName(@RequestParam String planetName){
        return spacePortService.getSpacePortsByPlanetName(planetName);
    }


    @PostMapping
    public SpacePortResponse createSpacePort(@RequestBody SpacePortRequest spacePortRequest){
        return spacePortService.createSpacePort(spacePortRequest);
    }

}
