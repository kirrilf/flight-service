package org.kirrilf.controller;


import lombok.RequiredArgsConstructor;
import org.kirrilf.dto.request.ShuttleRequest;
import org.kirrilf.dto.response.ShuttleResponse;
import org.kirrilf.service.ShuttleService;
import org.springframework.data.repository.query.Param;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/shuttle")
@RequiredArgsConstructor
public class ShuttleController {


    private final ShuttleService shuttleService;

    @GetMapping("/{shuttleName}")
    public ShuttleResponse getShuttle(@PathVariable String shuttleName,
                                      @RequestParam(required = false) Boolean withSeats){
        return shuttleService.getShuttleByName(shuttleName, withSeats);
    }

    @PostMapping
    public ShuttleResponse createShuttle(@RequestBody ShuttleRequest shuttleRequest){
        return shuttleService.createShuttle(shuttleRequest);
    }


}
