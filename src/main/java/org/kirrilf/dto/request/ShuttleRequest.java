package org.kirrilf.dto.request;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@Builder
@JsonIgnoreProperties(ignoreUnknown = true)
public class ShuttleRequest {

    private String shuttleName;

    private String shuttleCode;

    private String shuttleModel;

    private Integer shuttleRange;

    private List<String> seats;

}
