package org.kirrilf.dto.request;

public enum FindQueryType {
    SpacePortCode, Planet, SpacePortName
}
