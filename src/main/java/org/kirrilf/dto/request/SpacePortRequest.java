package org.kirrilf.dto.request;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
@JsonIgnoreProperties(ignoreUnknown = true)
public class SpacePortRequest {

    private String spacePortCode;

    private String spacePortName;

    private String planetName;



}
