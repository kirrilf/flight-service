package org.kirrilf.dto.request;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Builder;
import lombok.Data;


import java.util.Date;

@Data
@Builder
@JsonIgnoreProperties(ignoreUnknown = true)
public class FlightRequest {

    private String flightCode;

    private String spacePortCodeTo;

    private String spacePortCodeFrom;

    private Long departureTime;

    private Long arrivalTime;

    private String shuttleCode;

    private Integer price;

}
