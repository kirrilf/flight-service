package org.kirrilf.dto.request;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Builder;
import lombok.Data;

import java.time.LocalDate;

@Data
@Builder
@JsonIgnoreProperties(ignoreUnknown = true)
public class FindFlightRequest {

    String from;

    String to;

    FindQueryType type;

    LocalDate when;

    LocalDate dateRangeTo;

    LocalDate dateRangeBack;

    LocalDate whenBack;

    boolean needTicketBack;


}
