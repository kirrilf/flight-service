package org.kirrilf.dto.response;


import lombok.Builder;
import lombok.Data;

@Builder
@Data
public class SpacePortResponse {

    private String spacePortCode;

    private String spacePortName;

    private String planetName;

}
