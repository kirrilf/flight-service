package org.kirrilf.dto.response;

import lombok.Builder;
import lombok.Data;

import java.util.Date;

@Data
@Builder
public class FlightResponse {

    private String flightCode;

    private String spacePortCodeTo;

    private String spacePortCodeFrom;

    private Date actualDepartureTime;

    private Date actualArrivalTime;

    private Date scheduleDepartureTime;

    private Date scheduleArrivalTime;

    private String flightStatus;

    private String shuttleCode;

    private Integer price;


}
