package org.kirrilf.repository;

import org.kirrilf.model.Shuttle;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import java.util.Optional;

@EnableJpaRepositories
public interface ShuttleRepository extends JpaRepository<Shuttle, Long> {

    Optional<Shuttle> findShuttleByShuttleCode(String shuttleCode);

    Optional<Shuttle> findShuttleByShuttleName(String shuttleName);

}
