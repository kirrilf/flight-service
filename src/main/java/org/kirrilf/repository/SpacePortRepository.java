package org.kirrilf.repository;

import org.kirrilf.model.SpacePort;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface SpacePortRepository extends JpaRepository<SpacePort, Long> {

    Optional<SpacePort> findSpacePortBySpacePortCode(String spacePortCode);

    Optional<SpacePort> findSpacePortBySpacePortName(String spacePortName);

    Optional<List<SpacePort>> findSpacePortByPlanetName(String planetName);


}
