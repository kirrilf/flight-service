package org.kirrilf.repository;

import org.kirrilf.model.Seat;
import org.kirrilf.model.Shuttle;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import java.util.List;
import java.util.Optional;

@EnableJpaRepositories
public interface SeatRepository extends JpaRepository<Seat, Long> {

    List<Seat> findAllByShuttle(Shuttle shuttle);

}
