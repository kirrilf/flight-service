package org.kirrilf.repository;

import org.kirrilf.model.Flight;
import org.kirrilf.model.SpacePort;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.data.repository.query.Param;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@EnableJpaRepositories
public interface FlightRepository extends JpaRepository<Flight, Long> {


    Optional<Flight> findFlightByFlightCode(String flightCode);

    @Query(value = "from Flight f" +
            " where f.spacePortFrom in :from " +
            "and f.spacePortTo in :to " +
            "and f.scheduleDepartureTime between :start_day and :end_day")
    List<Flight> findFlightsBySpacePort(@Param("from") List<SpacePort> from,
                                     @Param("to") List<SpacePort> to,
                                     @Param("start_day") Date startDay,
                                     @Param("end_day") Date endDay);

    @Query(value = "from Flight f" +
            " where f.spacePortFrom = :from " +
            "and f.spacePortTo = :to " +
            "and f.scheduleDepartureTime between :start_day and :end_day")
    List<Flight> findFlightsByPlanetName(@Param("from") SpacePort from,
                                     @Param("to") SpacePort to,
                                     @Param("start_day") Date startDay,
                                     @Param("end_day") Date endDay);

}
