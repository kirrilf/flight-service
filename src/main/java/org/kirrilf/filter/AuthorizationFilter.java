package org.kirrilf.filter;

import lombok.RequiredArgsConstructor;
import org.kirrilf.serurity.TokenUtils;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@RequiredArgsConstructor
@Component
public class AuthorizationFilter extends OncePerRequestFilter {


    private final TokenUtils tokenUtils;

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
        String token = tokenUtils.resolveToken((HttpServletRequest) request);
        if (token != null && tokenUtils.validateToken(token)) {
            Authentication auth = tokenUtils.getAuthentication(token);
            if (auth != null) {
                SecurityContextHolder.getContext().setAuthentication(auth);
            }else{
                SecurityContextHolder.clearContext();
            }
        }else {
            SecurityContextHolder.clearContext();
        }
        filterChain.doFilter(request, response);
    }
}
