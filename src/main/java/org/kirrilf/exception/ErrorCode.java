package org.kirrilf.exception;

import lombok.RequiredArgsConstructor;
import lombok.ToString;

@RequiredArgsConstructor
@ToString
public enum ErrorCode {

    WRONG_FIND_PARAMETERS(3131, "Invalid search parameters"),

    WRONG_SEARCH_PARAM(2121, "Wrong search param"),

    TOO_MANY_REMAIN_SEATS(434, "Too many seats for this shuttle"),

    NOT_ENOUGH_SEATS(22, "Not enough seats in shuttle, you can wait(now don't work(need del job)) or choose another flight"),

    SHUTTLE_WITH_THIS_NAME_MISSING(21, "Shuttle with this name is missing")
    ;



    private final Integer code;
    private final String message;

}
