package org.kirrilf.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;



@ControllerAdvice
public class ApiExceptionHandler {

    @ExceptionHandler(value = {ApiRequestException.class})
    public ResponseEntity<Object> handleApiRequestException(ApiRequestException e){
       ApiException apiException = ApiException.builder()
               //.httpStatus(HttpStatus.BAD_REQUEST)
               .message(e.getMessage())
               //.timestamp(ZonedDateTime.now())
               .build();
        return new ResponseEntity<>(apiException, HttpStatus.BAD_REQUEST);
    }


}
