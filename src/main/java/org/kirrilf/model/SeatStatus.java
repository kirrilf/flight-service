package org.kirrilf.model;

public enum SeatStatus {
    BOOKED, FREE
}
