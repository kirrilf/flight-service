package org.kirrilf.model;

public enum ShuttleModel {
    CARGO, PASSENGER
}
