package org.kirrilf.model;


import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(name = "seats")
@Getter
@Setter
public class Seat extends BaseEntity{


    @ManyToOne(fetch = FetchType.LAZY)
    private Shuttle shuttle;

    @Enumerated(EnumType.STRING)
    @Column(name = "seat_status")
    private SeatStatus seatStatus;

    @Column(name = "seat")
    private String seat;


}
