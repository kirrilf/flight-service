package org.kirrilf.model;


import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Fetch;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "flights")
@Getter
@Setter
public class Flight extends BaseEntity{

    @Column(name = "flight_code", unique = true)
    private String flightCode;

    @OneToOne
    private SpacePort spacePortTo;

    @Column(name = "price")
    private Integer price;

    @OneToOne
    private SpacePort spacePortFrom;

    @Column(name = "schedule_departure_time")
    private Date scheduleDepartureTime;

    @Column(name = "schedule_arrival_time")
    private Date scheduleArrivalTime;

    @Column(name = "actual_departure_time")
    private Date actualDepartureTime;

    @Column(name = "actual_arrival_time")
    private Date actualArrivalTime;

    @Enumerated(EnumType.STRING)
    @Column(name = "flight_status")
    private FlightStatus flightStatus;

    @ManyToOne
    private Shuttle shuttle;

    @Column(name = "remain_seats")
    private Integer remainSeats;


}
