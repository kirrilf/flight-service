package org.kirrilf.model;

public enum FlightStatus {
    ARRIVED, DELAYED, SCHEDULED, CANCELLED, REGISTRATION, DEPARTED
}
