package org.kirrilf.model;


import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "space_ports")
@Getter
@Setter
public class SpacePort extends BaseEntity {

    @Column(name = "space_port_code", unique = true)
    private String spacePortCode;

    @Column(name = "space_port_name", unique = true)
    private String spacePortName;

    @Column(name = "planet_name")
    private String planetName;

}
