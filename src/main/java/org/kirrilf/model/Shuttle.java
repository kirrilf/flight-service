package org.kirrilf.model;

import lombok.*;
import org.hibernate.Hibernate;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "shuttle")
@Getter
@Setter
public class Shuttle extends BaseEntity{

    @Column(name = "shuttleName", unique = true)
    private String shuttleName;

    @Column(name = "shuttleCode", unique = true)
    private String shuttleCode;

    @Enumerated(EnumType.STRING)
    @Column(name = "model")
    private ShuttleModel model;

    @Column(name = "range")
    private Integer range;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        Shuttle shuttle = (Shuttle) o;
        return getId() != null && Objects.equals(getId(), shuttle.getId());
    }

    @Override
    public int hashCode() {
        return getClass().hashCode();
    }
}
