FROM adoptopenjdk:11-jre-hotspot
ARG JAR_FILE=*.jar
COPY target/flightService-2.5.4.jar flightServicee.jar
ENTRYPOINT ["java", "-jar", "flightServicee.jar"]
